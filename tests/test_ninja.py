from src.models.ninja_model import Ninja
from src.models.jutsu_model import Jutsu

def test_ninja():
    naruto = Ninja('Naruto', 'Uzumaki', 'Konoha')
    expected = {
        'name': 'Naruto', 
        'ninja_level': 'Unranked', 
        'clan': 'Uzumaki', 
        'village': 'Konoha', 
        'jutsu_list': [], 
        'health_pool': 100, 
        'chakra_pool': 100, 
        'concious': True
}   
    print(naruto.__dict__)


def test_ninja2():
    expected = "O ninja Naruto Uzumaki acabou de aprender um novo jutsu: Rasengan"
    
    # Criação de uma instância da classe Jutsu
    rasengan = Jutsu('Rasengan', 'Vento', 'a', 20, -15)

    # Criação de uma instância da classe Ninja
    naruto = Ninja('Naruto', 'Uzumaki', 'Konoha')

    #Chamada do método learn_jutsu
    res = naruto.learn_jutsu(rasengan)
    assert res == expected

def test_ninja3():
    expected = "O ninja Yuri Caetano acabou de aprender um novo jutsu: Programing"

    # Criação de uma instância da classe Jutsu
    programing = Jutsu('Programing', 'Computer', 'a', 20, 115)

    # Criação de uma instância da classe Ninja
    yuri = Ninja('Yuri', 'Caetano', 'Guatupe')

    #Chamada do método learn_jutsu
    res = yuri.learn_jutsu(programing)
    assert res == expected

    # Criação de uma outra instancia da classe Ninja
    sasuke = Ninja('Sasuke', 'Uchiha', 'Konoha')

    #Chamada do método cast_jutsu
    expected = False
    res = yuri.cast_jutsu(programing, sasuke)
    assert res == expected

    res = yuri.check_health(yuri)
    assert res == True