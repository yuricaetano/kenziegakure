from src.models.jounin_model import Jounin

def test_jounin():
    # Busca a melhor proficiência 1
    kakashi_proficiency = {'taijutsu': 20, 'ninjutsu': 8, 'genjutsu': 4}
    kakashi = Jounin('Yuri', 'Caetano', 'Konoha', kakashi_proficiency)
    expected = 'Yuri demonstra maior proficiência em taijutsu'
    res = kakashi.list_best_proficiency()
    assert res == expected

    # Vai fazer missão 1
    expected = 'O ninja Yuri Caetano saiu em missão'
    res = kakashi.start_mission()
    assert res == expected

    # Já está em missao 1
    expected = 'O ninja Yuri Caetano já está em uma missão'
    res = kakashi.start_mission()
    assert res == expected
   
    # Busca a melhor proficiência 2
    kakashi_proficiency = {'taijutsu': 7, 'ninjutsu': 9, 'genjutsu': 4}
    kakashi = Jounin('Kakashi', 'Hatake', 'Konoha', kakashi_proficiency)
    expected = 'Kakashi demonstra maior proficiência em ninjutsu'
    res = kakashi.list_best_proficiency()
    assert res == expected


    #Vai fazer missão 2 
    expected = 'O ninja Kakashi Hatake saiu em missão'
    res = kakashi.start_mission()
    assert res == expected

    #Já está em missao 2
    expected = 'O ninja Kakashi Hatake já está em uma missão'
    res = kakashi.start_mission()
    assert res == expected