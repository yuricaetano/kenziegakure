from src.models.jutsu_model import Jutsu
from src.models.ninja_model import Ninja

def test_jutsu():

    # 1 Create jutsu rasengan
    rasengan = Jutsu('Rasengan', 'Vento', 'a', 20, -15)
    
    # 2 Create ninja
    naruto = Ninja('Naruto', 'Uzumaki', 'Konoha')
    
    # 3 Ninja learn jutsu 
    naruto.learn_jutsu(rasengan)

    # 4 Create other ninja
    sasuke = Ninja('Sasuke', 'Uchiha', 'Konoha')


    # 5 Cast justu
    cast = naruto.cast_jutsu(rasengan, sasuke)
    assert cast == True
