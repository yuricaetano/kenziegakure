from src.models.ninja_model import Ninja
import operator

class Jounin(Ninja):

    habilities_keys = {"taijutsu": "", "ninjutsu": "", "genjutsu": ""}
    ninja_level = "Jounin"


    def __init__(self, 
                 name: str,
                 clan: str,
                 village: str,
                 proficiency = habilities_keys,
                 is_in_mission = False
        ):
        self.name = name
        self.clan = clan
        self.village = village
        self.proficiency = proficiency
        self.is_in_mission = is_in_mission

        Ninja.__init__(self,
                       name,
                       clan,
                       village,
                       ninja_level = "Jounin",
                       health_pool = 100,
                       chakra_pool = 100, 
                       concious = True,
        )

    def check_if_have_ninja_level():
        return "ola"

    def list_best_proficiency(self):
        best_proficiency = max(self.proficiency.items(), key=operator.itemgetter(1))[0]
        print("AQUI")
        print(best_proficiency)
        return f'{self.name} demonstra maior proficiência em {best_proficiency}'
    
    def start_mission(self):
        if self.is_in_mission == False:
            self.is_in_mission = True
            return f'O ninja {self.name} {self.clan} saiu em missão'
        if self.is_in_mission == True:
            self.is_in_mission = False
            return f'O ninja {self.name} {self.clan} já está em uma missão'
    
    def return_from_mission(self):
        #Caso não esteja em missão
        if self.is_in_mission == True:
            self.is_in_mission = False
            return f'O ninja {self.name} {self.clan} retornou em segurança da missão'

        if self.is_in_mission == False:
            return f'O ninja {self.name} {self.clan} não está em nenhuma missão no momento'

