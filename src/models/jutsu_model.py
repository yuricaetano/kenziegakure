class Jutsu:
    jutsu_ranks = ( 'D', 'C', 'B', 'A', 'S',)

    def __init__(self, 
                jutsu_name: str,
                jutsu_type: str, 
                jutsu_level: str, 
                jutsu_damage: int,
                chakra_spend: int):
                
        self.jutsu_name = jutsu_name
        self.jutsu_type = jutsu_type
        self.jutsu_level = self.verify_jutsu_level(jutsu_level)
        self.jutsu_damage = int(jutsu_damage)
        self.chakra_spend = self.is_positive(chakra_spend)


    def verify_jutsu_level(self, jutsu_level):

        not_in_rank = "Unranked"
        
        string_treatment = jutsu_level.upper()
        
        validation = [letters for letters in self.jutsu_ranks if string_treatment == letters]
        
        if len(validation) != 0:
            rank_string = validation[0]
            return rank_string
        else:
            return not_in_rank
    
    def is_positive(self, number: int):
        
        if number < 0:
            return 100
        if number > 0:
            return number


