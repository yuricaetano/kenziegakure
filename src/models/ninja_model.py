def check_if_have_jutsu(self, jutsu):
    self_jutsus = self.jutsu_list
    for self_jutsu in self_jutsus:
        self_dict_jutsu = self_jutsu.__dict__
        if self_dict_jutsu['jutsu_name'] == jutsu.jutsu_name:
            return True
        else:
            return False

def check_if_have_chackra(self, jutsu):
    chackra_pool = self.chakra_pool
    chakra_spend = jutsu.chakra_spend
    if chackra_pool >= chakra_spend:
        return True
    if chackra_pool < chakra_spend: 
        return False

class Ninja():
    
    def __init__(
        self,
        name: str, 
        clan: str,
        village: str, 
        ninja_level= "Unranked",
        health_pool = 100,
        chakra_pool = 100, 
        concious = True,
        ):

        self.name = name
        self.clan = clan
        self.village = village
        self.ninja_level = ninja_level
        self.jutsu_list = []
        self.health_pool = health_pool
        self.chakra_pool = chakra_pool
        self.concious = concious
    
    def learn_jutsu( self, jutsu ):
        self.jutsu_list.append(jutsu)
        return f'O ninja {self.name} {self.clan} acabou de aprender um novo jutsu: {jutsu.jutsu_name}'

    @staticmethod
    def check_health(ninja_to_check):
        life = ninja_to_check.health_pool
        if life < 0:
            ninja_to_check.concious = False
            ninja_to_check.health_pool = 0

        return ninja_to_check.concious
    

    def cast_jutsu(self, jutsu, adversary):
        if adversary.concious == False:
            return False
        else:
            if check_if_have_jutsu(self, jutsu) and check_if_have_chackra(self, jutsu):
                adversary.health_pool -= jutsu.jutsu_damage
                self.chakra_pool -= jutsu.chakra_spend
                return True
            else: return False


